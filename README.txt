File Reverser
Language: C
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 343 - Structure of Programming Languages
Semester / Year: Winter 2017

This program reverses a given file. For full specifications see 'proj1 desc.pdf'.

Compile with either gcc or clang on Linux.

Usage:
./reverse INPUT_FILENAME OUTPUT_FILENAME