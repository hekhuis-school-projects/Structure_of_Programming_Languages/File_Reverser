#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "file_utils.h"

/**************************************************************
 * Reads a text file and puts the contents of it into a
 * buffer of characters. Returns the size of the file
 * which is also the size of the buffer.
 * @param filename - filename of text file to read
 * @param **buffer - pointer to buffer of characters
 * @return -1 for failure to open file, otherwise size of file
 **************************************************************/
int read_file(char* filename, char **buffer){
	FILE *fptr;
	fptr = fopen(filename, "r");
	if (!fptr) {
		fprintf(stderr, "File failed to open.\n");
		return -1;
	}
	struct stat st;
	stat(filename, &st);
	int size = st.st_size;
	printf("File size is %d.\n", size);
	*buffer = malloc(size * sizeof(char));
	fread(*buffer, sizeof(char), size, fptr);
	printf("File read.\n"); 
	fclose(fptr);

	return size;
}

/************************************************************
 * Writes a buffer of characters to a passed file in reverse
 * so the end of the buffer is now at the beginning. Talked
 * to Gloire about doing the reversing of the file in the
 * write_file function or a reverse function in reverse.c.
 * Decided to do it in write_function to just write the buffer
 * backwards instead of manipulating the buffer to reverse
 * the contents inside of it.
 * @param filename - filename of file to save to
 * @param *buffer - buffer of characters to read from
 * @param size - size of the buffer
 * @return 0 if successful
 ***********************************************************/
int write_file(char* filename, char *buffer, int size){
	FILE *fptr;
	fptr = fopen(filename, "w");
	if (!fptr) {
		fprintf(stderr, "File failed to save.\n");
		return 1;
	}
	//Do -2 on starting position to get rid of \n at start of file
	for (int i = size - 2; i >= 0; i--) {
		fputc(buffer[i], fptr);
	}
	printf("File written.\n");
	fclose(fptr);

	return 0;
}
