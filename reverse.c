#include "file_utils.h"
#include <stdio.h>
#include <stdlib.h>

/*************************************************************
 * Takes an input text file and reverses its contents, then
 * prints them out to an output file. Gets the filenames
 * through command line arguements. argv[1] is input filename
 * and argv[2] is output filename.
 *************************************************************/
int main(int argc, char *argv[]) {
	char* buffer;
	int size = read_file(argv[1], &buffer);
	//Check for read_file error
	if (size == -1) {
		fprintf(stderr, "File read error.\n");
		return 1;
	}
	write_file(argv[2], buffer, size);

	return 0; 
}
